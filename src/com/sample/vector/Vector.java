package com.sample.vector;

public class Vector {
    int x;
    int y;

    Vector() {
        x = 0;
        y = 0;
    }

    Vector(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void add(Vector v) {
        this.x += v.x;
        this.y += v.y;
    }

    public void subtract(Vector v) {
        this.x -= v.x;
        this.y -= v.y;
    }
}
