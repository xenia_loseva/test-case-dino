package com.sample.vector;

import static org.junit.jupiter.api.Assertions.*;

class VectorTest {

    private Vector vector;

    @org.junit.jupiter.api.Test
    void add() {
        vector = new Vector();
        vector.add(new Vector(-1,1));
        assertEquals(-1, vector.x);
        assertEquals(1, vector.y);
    }

    @org.junit.jupiter.api.Test
    void subtract() {
        vector = new Vector();
        vector.subtract(new Vector(1, -1));
        assertEquals(-1, vector.x);
        assertEquals(1, vector.y);
    }
}